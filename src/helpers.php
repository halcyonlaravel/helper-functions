<?php

if (!function_exists('is_latest_mysql_version')) {
    /**
     * @deprecated assume always use latest mysql, deprecated because it access PDO on just running artisan
     */
    function is_latest_mysql_version(): bool
    {
        return true;
//        $pdo = DB::connection()->getPdo();
//        return ($pdo->getAttribute(PDO::ATTR_DRIVER_NAME) === 'mysql') &&
//            version_compare($pdo->getAttribute(PDO::ATTR_SERVER_VERSION), '5.7.8', 'ge');
    }
}

if (!function_exists('test_check_mysql')) {
    /**
     * @return array
     */
    function test_check_mysql()
    {
        $pdo = DB::connection()->getPdo();
        return [
            $pdo->getAttribute(PDO::ATTR_DRIVER_NAME) => ($pdo->getAttribute(PDO::ATTR_DRIVER_NAME) === 'mysql'),
            $pdo->getAttribute(PDO::ATTR_SERVER_VERSION) => version_compare(
                $pdo->getAttribute(PDO::ATTR_SERVER_VERSION),
                '5.7.8',
                'ge'
            ),
        ];
    }
}

if (!function_exists('test_file_path')) {
    /**
     * @param  string  $filePath
     *
     * @return string
     * @codeCoverageIgnore
     */
    function test_file_path(string $filePath)
    {
        return base_path('test_default_files'.DIRECTORY_SEPARATOR.$filePath);
    }
}

if (!function_exists('is_has_error')) {
    /**
     * @param  \Illuminate\Support\ViewErrorBag  $viewErrorBag
     * @param  string  $field
     * @param  string  $class
     *
     * @return string
     */
    function is_has_error(Illuminate\Support\ViewErrorBag $viewErrorBag, string $field, string $class = 'is-invalid')
    {
        return $viewErrorBag->has($field) ? ' '.$class : '';
    }
}

if (!function_exists('is_laravel')) {
    /**
     * @return bool
     */
    function is_laravel(): bool
    {
        return app() instanceof Illuminate\Foundation\Application;
    }
}

if (!function_exists('is_class_uses_deep')) {
    /**
     * @param $class
     * @param  string  $trait
     *
     * @return bool
     */
    function is_class_uses_deep($class, string $trait): bool
    {
        return in_array($trait, class_uses_recursive($class));
    }
}

if (!function_exists('touch_timestamp_model')) {
    /**
     * @param $object
     */
    function touch_timestamp_model($object)
    {
        if (is_class_uses_deep($object, Illuminate\Database\Eloquent\Concerns\HasTimestamps::class)) {
            /** @var Illuminate\Database\Eloquent\Concerns\HasTimestamps $object */
            $object->touch();
        }
    }
}