# Release Notes for 1.x

    2022-11-17 v1.1.0: deprecated is_latest_mysql_version(), it always return true, 
    2021-04-23 v1.0.3: Add support for PHP8, add changelog file
    2020-04-03 v1.0.2: Add touch_timestamp_model
    2020-03-06 v1.0.1: Remove not related
    2020-03-05 v1.0.0: Initial release